
SERVER ?= localhost

SUBJ_SERVER ?= "/CN=${SERVER}"
SUBJ_CA ?= "/C=SU/ST=Leningrad/L=Leningrad/O=Tehnokom/OU=IT Department/CN=universo.pro"

DAYS_CA ?= 10000
DAYS_SERVER ?= 5000

KEYS_CA ?= 2048
KEYS_SERVER ?= 2048

.PHONY: all clean CA server

all:
	@echo "usage:"
	@echo "	make CA"
	@echo "	make server SERVER=<domain.tld>"

clean:
	rm -rf out/

CA: out/ out/rootCA.crt

server: out/${SERVER}/ out/${SERVER}/server.key out/${SERVER}/fullchain.pem

out/:
	# out dir
	@test -d out/ || mkdir out/

out/${SERVER}/: out/
	# out/${SERVER} dir
	@test -d out/${SERVER}/ || mkdir out/${SERVER}/

# создание CA

out/rootCA.key:
	# приватный ключ CA
	@openssl genrsa ${KEYS_CA} \
		> out/rootCA.key 2> /dev/null

out/rootCA.crt: out/rootCA.key
	# сертификат CA (его ставить в браузер)
	@openssl req \
		-x509 \
		-key out/rootCA.key \
		-days ${DAYS_CA} \
		-subj ${SUBJ_CA} \
		> out/rootCA.crt 2>/dev/null

# создание сертификата сервера

out/${SERVER}/server.key:
	# приватный ключ сайта
	@openssl genrsa ${KEYS_SERVER} \
		> out/${SERVER}/server.key 2> /dev/null

out/${SERVER}/server.csr: out/${SERVER}/server.key
	# запрос сертификата сайта
	@openssl req \
		-new \
		-key out/${SERVER}/server.key \
		-subj ${SUBJ_SERVER} \
		-addext "subjectAltName = DNS:${SERVER}" \
		> out/${SERVER}/server.csr 2> /dev/null


# подписывание запроса сертификата

out/${SERVER}/ext_san.cnf:
	# конфиг для "хрома"
	@cat /etc/ssl/openssl.cnf >$@
	@echo "\n[SAN]\nsubjectAltName=DNS:${SERVER}" >>$@

out/${SERVER}/server.crt: out/${SERVER}/server.csr out/${SERVER}/ext_san.cnf
	# сертификат сайта
	@openssl x509 \
		-req \
		-in out/${SERVER}/server.csr \
		-CA out/rootCA.crt \
		-CAkey out/rootCA.key \
		-CAcreateserial \
		-days ${DAYS_SERVER} \
		-extfile out/${SERVER}/ext_san.cnf \
		-extensions SAN \
		> out/${SERVER}/server.crt 2> /dev/null


# цепочка публичных ключей

out/${SERVER}/fullchain.pem: out/rootCA.crt out/${SERVER}/server.crt
	# сертификат сервера + сертификат CA
	@cat out/${SERVER}/server.crt > out/${SERVER}/fullchain.pem
	@cat out/rootCA.crt >> out/${SERVER}/fullchain.pem


# тестирование сертификата для сервера localhost

up:
	docker-compose up

down:
	docker-compose down


test:
	curl --cacert out/rootCA.crt -i https://localhost:8443/
